=== Spice Starter Sites Plus ===

Contributors: 		spicethemes
Requires at least: 	5.3
Requires PHP: 		5.2
Tested up to: 		6.2.2
Stable tag: 		1.1.5
License: 			GPLv2 or later
License URI: 		https://www.gnu.org/licenses/gpl-2.0.html

== Description ==

The plugin allows you to create professional designed pixel perfect websites in minutes. Import the stater sites to create the beautiful websites.

== Installation ==

1. Upload `spice-starter-sites-plus` to the `/wp-content/plugins/` directory;
2. Activate the plugin through the 'Plugins > Installed Plugins' menu in WordPress dashboard;
3. Done!

== Changelog ==

@Version 1.1.5
* Updated freemius directory.

@Version 1.1.4
* Added new starter sites.

@Version 1.1.3
* Moved the starter site option into the OliveWP Plus menu from the Appearance menu.

@Version 1.1.2
* Added new starter sites.

@Version 1.1.1
* Freemius code updated.

@Version 1.1
* Added new starter sites.

@Version 1.0
* Added new starter site.

@Version 0.2
* Added new starter site.

@Version 0.1
* Initial Release.
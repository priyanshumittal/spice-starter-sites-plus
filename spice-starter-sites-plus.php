<?php
/*
Plugin Name:       	Spice Starter Sites Plus
Plugin URI:         https://olivewp.org/
Description:       	The plugin allows you to create professional designed pixel perfect websites in minutes. Import the stater sites to create the beautiful websites.
Version:           	1.1.5
Requires at least: 	5.3
Requires PHP: 		5.2
Tested up to: 		6.2.2
Author:            	spicethemes
Author URI:        	https://spicethemes.com
License: 			GPLv2 or later
License URI:       	http://www.gnu.org/licenses/gpl-2.0.txt
Text Domain:       	spice-starter-sites-plus
Domain Path:  		/languages
*/


if ( ! function_exists( 'sssp_fs' ) ) {
    // Create a helper function for easy SDK access.
    function sssp_fs() {
        global $sssp_fs;

        if ( ! isset( $sssp_fs ) ) {
            // Include Freemius SDK.
            if ( function_exists('olivewp_companion_activate') && defined( 'OWC_PLUGIN_DIR' ) && file_exists(OWC_PLUGIN_DIR . '/inc/freemius/start.php') ) {
                // Try to load SDK from olivewp companion folder.
                require_once OWC_PLUGIN_DIR . '/inc/freemius/start.php';
            } else if ( function_exists('olivewp_plus_activate') && defined( 'OLIVEWP_PLUGIN_DIR' ) && file_exists(OLIVEWP_PLUGIN_DIR . '/freemius/start.php') ) {
                // Try to load SDK from premium olivewp companion plugin folder.
                require_once OLIVEWP_PLUGIN_DIR . '/freemius/start.php';
            } else {
                require_once dirname(__FILE__) . '/freemius/start.php';
            }

            $sssp_fs = fs_dynamic_init( array(
                'id'                  => '10626',
                'slug'                => 'spice-starter-sites-plus',
                'type'                => 'plugin',
                'public_key'          => 'pk_90c30f60d64aaa76bc5407af43c7b',
                'is_premium'          => true,
                'is_premium_only'     => true,
                'has_paid_plans'      => true,
                'menu'                => array(
                    'support'        => false,
                ),
            ) );
        }

        return $sssp_fs;
    }
    sssp_fs();
}

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
		die;
}

/**
 * Set up and initialize
 */
class Spice_Starter_Sites_Plus {
		private static $instance;

		/**
		 * Actions setup
		 */
		public function __construct() {
			add_action( 'plugins_loaded', array( $this, 'constants' ), 2 );
			add_action( 'plugins_loaded', array( $this, 'includes' ), 4 );
			add_action( 'init', array( $this, 'load_plugin_textdomain' ) );
			//check if the One Click Demo Importer is installed or activated
		    if (!class_exists('OCDI_Plugin'))
		    {
	    		add_action('admin_notices', array( $this, 'admin_notice' ), 6 );
	   		 	return;
		    }
		}

		/**
		 * Constants
		 */
		function constants() {
			define( 'SPT_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
		}

		/*
	    * Admin Notice
	    * Warning when the site doesn't have One Click Demo Importer installed or activated    
	    */
		public function admin_notice() {
		  if (!class_exists('OCDI_Plugin')){
                echo '<div class="notice notice-warning is-dismissible"><p><strong>', esc_html__('"Spice Starter Sites Plus" requires "One Click Demo Import" to be installed and activated.','spice-starter-sites-plus'), '</strong></p>';
                if(function_exists('olivewp_plus_activate')){
                    $olivewp_plus_about_page=olivewp_plus_about_page();
                }
                else{
                    $olivewp_plus_about_page=olivewp_about_page();            
                }
                $olivewp_plus_actions = $olivewp_plus_about_page->recommended_actions;
                $olivewp_plus_actions_todo = get_option( 'recommended_actions', false );
                if($olivewp_plus_actions): 
                    foreach ($olivewp_plus_actions as $key => $olivewp_plus_val):
                        if($olivewp_plus_val['id']=='install_one-click-demo-import'):
                            /* translators: %s: theme name */
                            echo '<p>'.wp_kses_post($olivewp_plus_val['link']).'</p></div>';
                        endif;
                    endforeach;
                endif;
            }
		}

		/**
		 * Includes
		 */
		function includes() {
				require_once( SPT_DIR . 'demo-content/setup.php' );
		}

		static function install() {
			if ( version_compare(PHP_VERSION, '5.4', '<=') ) {
				wp_die( __( 'Spice Starter Sites Plus requires PHP 5.4. Please contact your host to upgrade your PHP. The plugin was <strong>not</strong> activated.', 'spice-starter-sites-plus' ) );
			};

		}

		/**
		 * Returns the instance.
		 */
		public static function get_instance() {

			if ( !self::$instance )
					self::$instance = new self;
			return self::$instance;
		}

		/**
		 * Load the localisation file.
		*/
		public function load_plugin_textdomain() {
			load_plugin_textdomain( 'spice-starter-sites-plus' , false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
		}
}

function spice_starter_sites_plus_plugin(){
	return Spice_Starter_Sites_Plus::get_instance();
}
add_action('plugins_loaded', 'spice_starter_sites_plus_plugin', 1);

//Does not activate the plugin on PHP less than 5.4
register_activation_hook( __FILE__, array( 'Spice_Starter_Sites_Plus', 'install' ) );

// Notice to activate plugin
function sssp_admin_notice_success() {
    if ( get_option( 'dismissed-sssp_success_plugin', false ) ) {
       return;
    }
    if ( function_exists('olivewp_plus_activate')) {
        return;
    }?>
    <div class="updated notice is-dismissible sssp-success" >
	    <p style="font-size:14px;"><?php printf(_e( 'To import the premium <strong> Starter Sites </strong> demo successfully firstly you need to activate the <strong> OliveWP Plus </strong> plugin.', 'spice-starter-sites-plus' )); ?>
	    </p>
	</div>
    <script type="text/javascript">
        jQuery(function($) {
        $( document ).on( 'click', '.sssp-success .notice-dismiss', function () {
            var type = $( this ).closest( '.sssp-success' ).data( 'notice' );
            $.ajax( ajaxurl,
              {
                type: 'POST',
                data: {
                  action: 'dismissed_sssp_plugin_success_handler',
                  type: type,
                }
              } );
          } );
      });
    </script>
    <?php
}
add_action( 'admin_notices', 'sssp_admin_notice_success' );

function sssp_success_ajax_handler() {
    // Store it in the options table
    update_option( 'dismissed-sssp_success_plugin', TRUE );
}
add_action( 'wp_ajax_dismissed_sssp_plugin_success_handler', 'sssp_success_ajax_handler');
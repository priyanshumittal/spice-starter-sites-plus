<?php
/**
 * @package Spice Starter Sites Plus
 * @since 1.0
 */


/**
 * Set import files
 */
if ( !function_exists( 'spice_starter_sites_plus_import_files' ) ) {

    function spice_starter_sites_plus_import_files() {

        // Demos url
        $demo_url = 'https://olivewp.org/owp/startersites/';

        return array(
            //Default Demo
            array(
                'import_file_name'              =>  esc_html__('Business', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'business/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'business/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'business/customize-export.dat',
                'preview_url'                   =>  'https://owp-business.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/business/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Travel', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'travel/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'travel/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'travel/customize-export.dat',
                'preview_url'                   =>  'https://owp-travel.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/travel/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Spacare', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'spacare/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'spacare/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'spacare/customize-export.dat',
                'preview_url'                   =>  'https://owp-spacare.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/spacare/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Interior', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'interior/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'interior/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'interior/customize-export.dat',
                'preview_url'                   =>  'https://owp-interior.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/interior/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Real Estate', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'realestate/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'realestate/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'realestate/customize-export.dat',
                'preview_url'                   =>  'https://owp-realestate.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/realestate/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Skin Spa', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'skinspa/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'skinspa/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'skinspa/customize-export.dat',
                'preview_url'                   =>  'https://owp-skinspa.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/skinspa/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Healthcare', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'healthcare/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'healthcare/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'healthcare/customize-export.dat',
                'preview_url'                   =>  'https://owp-healthcare.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/healthcare/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Gymer', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'gymer/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'gymer/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'gymer/customize-export.dat',
                'preview_url'                   =>  'https://owp-gymer.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/gymer/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Yoga', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'yoga/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'yoga/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'yoga/customize-export.dat',
                'preview_url'                   =>  'https://owp-yoga.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/yoga/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Massage', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'massagecenter/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'massagecenter/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'massagecenter/customize-export.dat',
                'preview_url'                   =>  'https://owp-massage.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/massagecenter/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Finance', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'finance/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'finance/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'finance/customize-export.dat',
                'preview_url'                   =>  'https://owp-finance.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/finance/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Industry', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'industry/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'industry/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'industry/customize-export.dat',
                'preview_url'                   =>  'https://owp-industry.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/industry/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Coffee Shop', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'coffee-shop/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'coffee-shop/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'coffee-shop/customize-export.dat',
                'preview_url'                   =>  'https://owp-coffee-shop.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/coffee-shop/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Insurance', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'insurance/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'insurance/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'insurance/customize-export.dat',
                'preview_url'                   =>  'https://owp-insurance.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/insurance/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Creative Agency', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'creative-agency/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'creative-agency/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'creative-agency/customize-export.dat',
                'preview_url'                   =>  'https://owp-creative-agency.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/creative-agency/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Adventure', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'adventure/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'adventure/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'adventure/customize-export.dat',
                'preview_url'                   =>  'https://owp-adventure.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/adventure/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Carpenter', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'carpenter/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'carpenter/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'carpenter/customize-export.dat',
                'preview_url'                   =>  'https://owp-carpenter.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/carpenter/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Startup Agency', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'startup-agency/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'startup-agency/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'startup-agency/customize-export.dat',
                'preview_url'                   =>  'https://owp-startup-agency.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/startup-agency/thumb.png',
            ),
             array(
                'import_file_name'              =>  esc_html__('Jewel', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'jewel/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'jewel/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'jewel/customize-export.dat',
                'preview_url'                   =>  'https://owp-jewel.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/jewel/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Wedding', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'wedding/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'wedding/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'wedding/customize-export.dat',
                'preview_url'                   =>  'https://owp-wedding.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/wedding/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Pet Care', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'pet-care/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'pet-care/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'pet-care/customize-export.dat',
                'preview_url'                   =>  'https://owp-pet-care.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/pet-care/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Sports', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'sport/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'sport/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'sport/customize-export.dat',
                'preview_url'                   =>  'https://owp-sports.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/sport/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Music', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'music/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'music/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'music/customize-export.dat',
                'preview_url'                   =>  'https://owp-music.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/music/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Lawyer', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'lawyer/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'lawyer/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'lawyer/customize-export.dat',
                'preview_url'                   =>  'https://owp-lawyer.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/lawyer/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Automobile', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'automobile/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'automobile/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'automobile/customize-export.dat',
                'preview_url'                   =>  'https://owp-automobile.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/automobile/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Event Management', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'event-management/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'event-management/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'event-management/customize-export.dat',
                'preview_url'                   =>  'https://owp-event-management.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/event-management/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Barber Shop', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'barber-shop/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'barber-shop/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'barber-shop/customize-export.dat',
                'preview_url'                   =>  'https://owp-barber-shop.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/barber-shop/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Hotel and Restaurant', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'hotel-restro/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'hotel-restro/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'hotel-restro/customize-export.dat',
                'preview_url'                   =>  'https://owp-hotel-restro.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/hotel-restro/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Education', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'education/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'education/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'education/customize-export.dat',
                'preview_url'                   =>  'https://owp-education.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/education/thumb.png',
            ),
            array(
                'import_file_name'              =>  esc_html__('Restaurant', 'spice-starter-sites-plus'),
                'import_file_url'               =>  $demo_url . 'restaurant/sample-data.xml',
                'import_widget_file_url'        =>  $demo_url . 'restaurant/widgets.wie',
                'import_customizer_file_url'    =>  $demo_url . 'restaurant/customize-export.dat',
                'preview_url'                   =>  'https://owp-restaurant.olivewp.org/',
                'import_preview_image_url'      =>  $demo_url . 'thumbnail/restaurant/thumb.png',
            )

        );
    }

}
add_filter( 'pt-ocdi/import_files', 'spice_starter_sites_plus_import_files' );

/**
 * Define actions that happen after import
 */
if ( !function_exists( 'spice_starter_sites_plus_after_import_mods' ) ) {

    function spice_starter_sites_plus_after_import_mods() {

        //Assign the menu
        $main_menu = get_term_by( 'name', 'Primary Menu', 'nav_menu' );
        $footer_menu = get_term_by( 'name', 'Footer Menu', 'nav_menu' );
        set_theme_mod( 'nav_menu_locations', array('primary' => $main_menu->term_id, 'footer_menu' => $footer_menu->term_id ) );

        //Asign the static front page and the blog page
        $front_page = get_page_by_title( 'Home' );
        $blog_page  = get_page_by_title( 'Blog' );

        update_option( 'show_on_front', 'page' );
        update_option( 'page_on_front', $front_page -> ID );
        update_option( 'page_for_posts', $blog_page -> ID );

    }

}
add_action( 'pt-ocdi/after_import', 'spice_starter_sites_plus_after_import_mods' );

// Custom CSS for OCDI plugin
function spice_starter_sites_plus_ocdi_css() { ?>
    <style >
        .ocdi .ocdi__theme-about, .ocdi__intro-text {
            display: none;
        }
    </style>
<?php }
add_action('admin_enqueue_scripts', 'spice_starter_sites_plus_ocdi_css');

// Change the "One Click Demo Import" name from "Starter Sites" in Appearance menu
function spice_starter_sites_plus_ocdi_plugin_page_setup( $default_settings ) {

    $default_settings['parent_slug'] = 'admin.php';
    $default_settings['page_title']  = esc_html__( 'One Click Demo Import' , 'spice-starter-sites-plus' );
    $default_settings['menu_title']  = esc_html__( 'Starter Sites' , 'spice-starter-sites-plus' );
    $default_settings['capability']  = 'import';
    $default_settings['menu_slug']   = 'one-click-demo-import';

    return $default_settings;

}
add_filter( 'ocdi/plugin_page_setup', 'spice_starter_sites_plus_ocdi_plugin_page_setup' );

// Register required plugins for the demo's
function spice_starter_sites_plus_register_plugins( $plugins ) {

    // List of plugins used by all theme demos.
    $theme_plugins = [
        [   
            'name'     =>   'Elementor',
            'slug'     =>   'elementor',
            'required' =>   true,
        ],
        [ 
            'name'     =>   'Contact Form 7', 
            'slug'     =>   'contact-form-7',
            'required' =>   true,     
        ],
        [ 
            'name'     =>   'Yoast SEO',
            'slug'     =>   'wordpress-seo',
            'required' =>   true,
        ],
        [ 
            'name'     =>   'Unique Headers',
            'slug'     =>   'unique-headers',
            'required' =>   true,
        ]
    ];

    // Check if user is on the theme recommeneded plugins step and a demo was selected.
    if (isset( $_GET['step'] ) && $_GET['step'] === 'import' &&  isset( $_GET['import'] )) {
        if ( $_GET['import'] === '0' || $_GET['import'] === '2' || $_GET['import'] === '10' ) {
            $theme_plugins[] = [
                'name'     => 'Spice Portfolio',
                'slug'     => 'spice-portfolio',
                'required' => true,
            ]; 
            $theme_plugins[] = [
                'name'     => 'Spice Sticky Header',
                'slug'     => 'spice-sticky-header',
                'required' => true,
            ];
             $theme_plugins[] = [
                'name'     => 'Spice Sticky Footer',
                'slug'     => 'spice-sticky-footer',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '3' || $_GET['import'] === '5' || $_GET['import'] === '8' ) {

            $theme_plugins[] = [
                'name'     => 'Spice Sticky Header',
                'slug'     => 'spice-sticky-header',
                'required' => true,
            ];
            $theme_plugins[] = [
                'name'     => 'Spice Sticky Footer',
                'slug'     => 'spice-sticky-footer',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '4' || $_GET['import'] === '6' || $_GET['import'] === '7' || $_GET['import'] === '9' || $_GET['import'] === '19' || $_GET['import'] === '21' || $_GET['import'] === '22' || $_GET['import'] === '25' ) {

            $theme_plugins[] = [
                'name'     => 'Spice Sticky Header',
                'slug'     => 'spice-sticky-header',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '10' || $_GET['import'] === '0' || $_GET['import'] === '15' || $_GET['import'] === '16' || $_GET['import'] === '17' || $_GET['import'] === '20' || $_GET['import'] === '22' || $_GET['import'] === '23' || $_GET['import'] === '24' || $_GET['import'] === '26' || $_GET['import'] === '27' || $_GET['import'] === '28' || $_GET['import'] === '29' ) {

            $theme_plugins[] = [
                'name'     => 'Spice Post Slider',
                'slug'     => 'spice-post-slider',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '1' || $_GET['import'] === '11' || $_GET['import'] === '17' || $_GET['import'] === '25' || $_GET['import'] === '26' ) {

            $theme_plugins[] = [
                'name'     => 'Spice Portfolio',
                'slug'     => 'spice-portfolio',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '9' || $_GET['import'] === '5' ) {

            $theme_plugins[] = [
                'name'     => 'WooCommerce',
                'slug'     => 'woocommerce',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '12' ) {

            $theme_plugins[] = [
                'name'     => 'Spice Side Panel',
                'slug'     => 'spice-side-panel',
                'required' => true,
            ];
            $theme_plugins[] = [
                'name'     => 'Spice Sticky Footer',
                'slug'     => 'spice-sticky-footer',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '13' || $_GET['import'] === '15' || $_GET['import'] === '16' || $_GET['import'] === '17' || $_GET['import'] === '19' || $_GET['import'] === '20' || $_GET['import'] === '21' || $_GET['import'] === '22' || $_GET['import'] === '23' || $_GET['import'] === '25' || $_GET['import'] === '27' ) {

            $theme_plugins[] = [
                'name'     => 'Spice Social Share',
                'slug'     => 'spice-social-share',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '13' || $_GET['import'] === '21' ) {

            $theme_plugins[] = [
                'name'     => 'Spice Sticky Footer',
                'slug'     => 'spice-sticky-footer',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '18' || $_GET['import'] === '19' || $_GET['import'] === '20' || $_GET['import'] === '22' || $_GET['import'] === '23' || $_GET['import'] === '24' || $_GET['import'] === '25' || $_GET['import'] === '26' || $_GET['import'] === '28' || $_GET['import'] === '29' ) {

            $theme_plugins[] = [
                'name'     => 'Spice Side Panel',
                'slug'     => 'spice-side-panel',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '18' || $_GET['import'] === '19' || $_GET['import'] === '20' || $_GET['import'] === '23' || $_GET['import'] === '24' || $_GET['import'] === '25' || $_GET['import'] === '26' || $_GET['import'] === '27' || $_GET['import'] === '28' || $_GET['import'] === '29' ) {

            $theme_plugins[] = [
                'name'     => 'ElementsKit Lite',
                'slug'     => 'elementskit-lite',
                'required' => true,
            ];
        }
        if ( $_GET['import'] === '22' ) {

            $theme_plugins[] = [
                'name'     => 'MP3 Audio Player by Sonaar',
                'slug'     => 'sonaar-music',
                'required' => true,
            ];
            $theme_plugins[] = [
                'name'     => 'Paymattic - Payments made simple',
                'slug'     => 'wppayform',
                'required' => true,
            ];
        }
    }

    return array_merge( $plugins, $theme_plugins );

}
add_filter( 'ocdi/register_plugins', 'spice_starter_sites_plus_register_plugins' );

/**
* Remove branding
*/
add_filter( 'pt-ocdi/disable_pt_branding', '__return_true' );